# Wally

Wally is a small personnal smart home project!

## Features

Wally is currently almost empty but the expected features are the following.

### Task List and Task Management
Create tasks, list of tasks and assign them days you want to do those tasks.
### Calendar Synchronisation
Synchronise with your calendar to get reminders of events.
### Good morning/Good night commands
Good morning gives you insight on what your day will look like. Meteo, meetings, etc... 

Goodnight is almost the same but it tells you if you have a meeting early in the morning or stuff like that.
### Voice commands
Almost everything is expected to react to voice commands so you can use Wally as a smart home tool!