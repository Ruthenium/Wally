const express = require(`express`);
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);

app.use('/public', express.static(__dirname + '/public'));

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/views/index.html');
});

io.on('connection', function (socket) {
  socket.emit('news', { hello: 'world' });
  socket.on('my other event', function (data) {
    console.log(data);
  });
});

server.listen(80);
console.log("Server running on :80!");