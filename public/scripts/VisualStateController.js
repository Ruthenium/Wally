"use strict";
class VisualStateController {
    constructor(defaultState) {
        const container = document.getElementById(VisualStateController.CONTAINER_ID);
        if (container) {
            this._container = container;
        }
        else {
            console.error(`Element "${VisualStateController.CONTAINER_ID}" cannot be found!`);
            return;
        }
        this._elements = new Map();
        for (var i = 0; i < this._container.children.length; i++) {
            const element = this._container.children.item(i);
            if (element.classList.contains(VisualStateController.ELEMENT_CLASS)) {
                // Make sure to disable all the current state
                element.classList.add(VisualStateController.INACTIVE_ELEMENT_CLASS);
                this._elements.set(element.id, element);
            }
        }
        this.SetState(defaultState);
    }
    SetState(newState) {
        if (!this._elements.has(newState)) {
            console.error(`Cannot switch to state "${newState} because no element matches it!`);
            return;
        }
        if (this._currentElement && this._currentElement.classList.contains(VisualStateController.ACTIVE_ELEMENT_CLASS)) {
            this.DisableState(this._currentElement);
        }
        this._currentElement = this._elements.get(newState);
        this.EnableState(this._currentElement);
    }
    DisableState(state) {
        state.classList.remove(VisualStateController.ACTIVE_ELEMENT_CLASS);
        state.classList.add(VisualStateController.INACTIVE_ELEMENT_CLASS);
    }
    EnableState(state) {
        state.classList.remove(VisualStateController.INACTIVE_ELEMENT_CLASS);
        state.classList.add(VisualStateController.ACTIVE_ELEMENT_CLASS);
    }
}
VisualStateController.CONTAINER_ID = `visual-state-container`;
VisualStateController.ELEMENT_CLASS = `visual-state`;
VisualStateController.ACTIVE_ELEMENT_CLASS = `active-visual-state`;
VisualStateController.INACTIVE_ELEMENT_CLASS = `inactive-visual-state`;
