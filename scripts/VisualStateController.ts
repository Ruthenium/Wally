class VisualStateController {
    private static CONTAINER_ID = `visual-state-container`;
    private static ELEMENT_CLASS = `visual-state`;
    private static ACTIVE_ELEMENT_CLASS = `active-visual-state`;
    private static INACTIVE_ELEMENT_CLASS = `inactive-visual-state`;

    private _container: HTMLElement;
    private _elements: Map<string, HTMLElement>;
    private _currentElement: HTMLElement;

    public constructor(defaultState: string) {
        const container = document.getElementById(VisualStateController.CONTAINER_ID);
        if (container) {
            this._container = container;
        } else {
            console.error(`Element "${VisualStateController.CONTAINER_ID}" cannot be found!`);
            return;
        }
        this._elements = new Map<string, HTMLElement>();

        for (var i = 0; i < this._container.children.length; i++) {
            const element = this._container.children.item(i) as HTMLElement;
            if (element.classList.contains(VisualStateController.ELEMENT_CLASS)) {
                // Make sure to disable all the current state
                element.classList.add(VisualStateController.INACTIVE_ELEMENT_CLASS);
                this._elements.set(element.id, element);
            }
        }

        this.SetState(defaultState);
    }

    public SetState(newState: string) {
        if (!this._elements.has(newState)) {
            console.error(`Cannot switch to state "${newState} because no element matches it!`);
            return;
        }

        if (this._currentElement && this._currentElement.classList.contains(VisualStateController.ACTIVE_ELEMENT_CLASS)) {
            this.DisableState(this._currentElement);
        }

        this._currentElement = this._elements.get(newState) as HTMLElement;
        this.EnableState(this._currentElement);
    }

    private DisableState(state: HTMLElement) {
        state.classList.remove(VisualStateController.ACTIVE_ELEMENT_CLASS);
        state.classList.add(VisualStateController.INACTIVE_ELEMENT_CLASS);
    }

    private EnableState(state: HTMLElement) {
        state.classList.remove(VisualStateController.INACTIVE_ELEMENT_CLASS);
        state.classList.add(VisualStateController.ACTIVE_ELEMENT_CLASS);
    }
}