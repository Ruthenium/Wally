let visualStateController: VisualStateController;
let resetWelcomeHandle: number;

window.onload = () => {
    visualStateController = new VisualStateController(`welcome-page`);
    InitializeAnnyang();
}

function InitializeAnnyang() {
    annyang.setLanguage("fr-FR");

    annyang.addCommands({
        "Bonjour Wally": OnWelcome,
        "Bonjour Wall-E": OnWelcome,
        "Bonjour Ouali": OnWelcome,
        "Bonjour Wallie": OnWelcome,
        "Bonjour Wahly": OnWelcome,
    })

    annyang.start({ autoRestart: true, continuous: false });
}

function OnWelcome() {
    annyang.pause();
    annyang.removeCommands();
    annyang.removeCallback();
    annyang.addCallback("result", (phrases: string[]) => {
        const resultContainer = document.getElementById(`echo-commands`) as HTMLElement;
        const childNodes = resultContainer.childNodes;
        const toRemove: HTMLElement[] = [];
        for (const node of childNodes) {
            if (node instanceof HTMLElement) {
                const htmlNode = node as HTMLElement;
                if (htmlNode.classList.contains(`annyang-result`)) {
                    toRemove.push(htmlNode);
                }
            }
        }

        for (const nodeToRemove of toRemove) {
            resultContainer.removeChild(nodeToRemove);
        }

        for (const phrase of phrases) {
            const phraseElement = document.createElement("p");
            phraseElement.classList.add(`annyang-result`);
            phraseElement.innerText = "\"" + phrase + "\"";
            resultContainer.appendChild(phraseElement);
        }

        StartResetWelcome();
    });

    annyang.resume();

    visualStateController.SetState(`echo-commands`);
    StartResetWelcome();
}

function StartResetWelcome() {
    if (resetWelcomeHandle) clearTimeout(resetWelcomeHandle);
    resetWelcomeHandle = setTimeout(() => {
        visualStateController.SetState(`welcome-page`);
        annyang.pause();
        annyang.removeCallback();
        annyang.removeCommands();
        annyang.addCommands({
            "Bonjour Wally": OnWelcome,
            "Bonjour Wall-E": OnWelcome,
            "Bonjour Ouali": OnWelcome,
            "Bonjour Wallie": OnWelcome,
            "Bonjour Wahly": OnWelcome,
            "Bonjour Poli": OnWelcome,
        })
        annyang.resume();
    }, 10000);
}