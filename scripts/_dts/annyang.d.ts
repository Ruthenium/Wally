export as namespace annyang;

export function init(commands : commands, resetCommands?: boolean) : void;
export function start(options?: startOptions) : void;
export function abort() : void;
export function pause() : void;
export function resume() : void;
export function debug(newState?: boolean) : void;
export function setLanguage(language: string) : void;
export function addCommands(commands : commands) : void;
export function removeCommands(commandsToRemove? : string|string[]) : void;
export function addCallback(type: "start" | "soundstart" | "end", callback : emptyCallback, context? : any) : void;
export function addCallback(type: "error" | "errorNetwork" | "errorPermissionBlocked" | "errorPermissionDenied", callback : errorCallback, context? : any) : void;
export function addCallback(type: "result" | "resultNoMatch", callback: resultCallback, context? : any): void;
export function addCallback(type: "resultMatch", callback : resultMatchCallback, context? : any) : void; 
export function removeCallback() : void;
export function removeCallback(type : keyof callbackTypes) : void;
export function removeCallback(type: "start" | "soundstart" | "end", callback : emptyCallback, context? : any) : void;
export function removeCallback(type: "error" | "errorNetwork" | "errorPermissionBlocked" | "errorPermissionDenied", callback : errorCallback, context? : any) : void;
export function removeCallback(type: "result" | "resultNoMatch", callback: resultCallback, context? : any): void;
export function removeCallback(type: "resultMatch", callback : resultMatchCallback, context? : any) : void; 
export function isListening() : boolean;
export function getSpeechRecognizer() : any;
export function trigger(toTrigger : string|string[]) : void;
 

interface emptyCallback { () : void; }
interface errorCallback { (errorEvent?: any) : void }
interface resultCallback { (phrases? : string[]) : void }
interface resultMatchCallback { (userSaid?: string, commandText?: string, phrases? : string[]): void}

interface callbackTypes {
    start : string,
    soundstart : string,
    error : string,
    errorNetwork : string,
    errorPermissionBlocked : string,
    errorPermissionDenied : string,
    end : string,
    result : string,
    resultMatch : string,
    resultNoMatch : string,
}

export interface startOptions {
    autoRestart?: boolean;
    continuous?: boolean;
    paused?:boolean;
}

export interface commands {
    [index : string] : Function | commandObject
}

export interface commandObject {
    regexp : RegExp
    callback : Function
}
